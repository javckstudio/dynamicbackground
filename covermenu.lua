-----------------------------------------------------------------------------------------
--
-- covermenu.lua
--
-----------------------------------------------------------------------------------------
--=======================================================================================
--引入各種函式庫
--=======================================================================================
local widget = require( "widget" )
local composer = require( "composer" )
local scene = composer.newScene( )
--=======================================================================================
--宣告各種變數
--=======================================================================================
local bg
local img_cloud1
local img_cloud2
local img_cloud3
local img_building1
local img_building2
local img_road1
local img_road2
local btn_play
local btn_more
local btn_gameCenter
local img_title
local ufo = display.newGroup( )
local stopMovingUfo
local iPhone5AddOn
local bgMusic = audio.loadStream( "BackgroundMusic1.mp3" )
local buttonPressedSound = audio.loadSound( "ButtonPressed.mp3" )
local goingUpLongSound = audio.loadSound( "GoingUpLong.mp3" )
local isIPhone5 = false
if(system.getInfo("model") == "iPhone" or system.getInfo( "model" ) == "iPod touch") then
	isIPhone5 = (display.pixelHeight > 960)
end
local changeSceneEffect = {
	effect = "crossFade",
	time = 300
}

local initial
local playButtonPressed
local moreAppButtonPressed
local gameCenterButtonPressed
local ufoMove1
local ufoMove2
local moveMyStuff
local moveMyStuffNormal
--=======================================================================================
--定義各種函式
--=======================================================================================
initial = function ( parent )
	if isIPhone5 then
		iPhone5AddOn = 68
	else
		iPhone5AddOn = 0
	end

	bg = display.newRect( 0 , 0 , _SCREEN.WIDTH, _SCREEN.HEIGHT )
	bg.x = _SCREEN.CENTER.X
	bg.y = _SCREEN.CENTER.Y
	bg:setFillColor( 0.22 , 0.8 , 1 )
	parent:insert( bg )

	img_cloud3 = display.newImageRect( "Cloud.png", 100, 64 )
	img_cloud3.x = 497
	img_cloud3.y = 327
	img_cloud3.speed = 0.5
	img_cloud3.enterFrame = moveMyStuffNormal
	parent:insert( img_cloud3 )

	img_cloud2 = display.newImageRect( "Cloud.png", 126, 86 )
	img_cloud2.x = 448
	img_cloud2.y = 88
	img_cloud2.speed = 1
	img_cloud2.enterFrame = moveMyStuffNormal
	parent:insert( img_cloud2 )

	img_cloud1 = display.newImageRect( "Cloud.png", 166, 115 )
	img_cloud1.x = 120
	img_cloud1.y = 154
	img_cloud1.speed = 1.5
	img_cloud1.enterFrame = moveMyStuffNormal
	parent:insert( img_cloud1 )

	img_building1 = display.newImageRect( "Building.png", 640, 242 )
	img_building1.x = _SCREEN.CENTER.X
	img_building1.y = _SCREEN.HEIGHT - 280
	img_building1.speed = 4
	img_building1.enterFrame = moveMyStuffNormal
	parent:insert( img_building1 )

	img_building2 = display.newImageRect( "Building.png", 640, 242 )
	img_building2.x = _SCREEN.CENTER.X + _SCREEN.WIDTH
	img_building2.y = _SCREEN.HEIGHT - 280
	img_building2.speed = 4
	img_building2.enterFrame = moveMyStuffNormal
	parent:insert( img_building2 )

	img_road1 = display.newImageRect( "Road.png", 640, 166 )
	img_road1.x = _SCREEN.CENTER.X
	img_road1.y = _SCREEN.HEIGHT - 83
	img_road1.speed = 8
	img_road1.enterFrame = moveMyStuffNormal
	parent:insert( img_road1 )

	img_road2 = display.newImageRect( "Road.png", 640, 166 )
	img_road2.x = _SCREEN.CENTER.X + _SCREEN.WIDTH
	img_road2.y = _SCREEN.HEIGHT - 83
	img_road2.speed = 8
	img_road2.enterFrame = moveMyStuffNormal
	parent:insert( img_road2 )

	btn_play = widget.newButton{
		width = 228,
		height = 108,
		defaultFile = "PlayButton.png",
		overFile = "PlayButtonPressed.png",
		onEvent = playButtonPressed
	}
	btn_play.x = _SCREEN.CENTER.X - 138
	btn_play.y = _SCREEN.CENTER.Y + 60 + iPhone5AddOn
	parent:insert( btn_play )

	btn_more = widget.newButton{
		width = 228,
		height = 108,
		defaultFile = "MoreAppButton.png",
		overFile = "MoreAppButtonPressed.png",
		onEvent = moreAppButtonPressed
	}
	btn_more.x = _SCREEN.CENTER.X + 138
	btn_more.y = _SCREEN.CENTER.Y + 60 + iPhone5AddOn
	parent:insert( btn_more )

	--btn_gameCenter = display.newImageRect( "GameCenter.png", 100, 100 )
	btn_gameCenter = widget.newButton{
		width = 100,
		height = 100,
		defaultFile = "GameCenter.png",
		overFile = "GameCenterPressed.png",
		onEvent = gameCenterButtonPressed
	}
	btn_gameCenter.x = _SCREEN.CENTER.X
	btn_gameCenter.y = _SCREEN.CENTER.Y + 178 + iPhone5AddOn
	parent:insert( btn_gameCenter )

	img_title = display.newImageRect( "CoverTitle.png", 556, 61 )
	img_title.x = _SCREEN.CENTER.X
	img_title.y = _SCREEN.CENTER.Y - 46 + iPhone5AddOn
	parent:insert( img_title )

	local tempUfo1 = display.newImageRect( "UFO1.png", 154, 150 )
	local tempUfo2= display.newImageRect( "UFO2.png", 154, 150 )
	ufo:insert( tempUfo1 )
	ufo:insert( tempUfo2 )
	ufo.x = _SCREEN.CENTER.X
	ufo.y = _SCREEN.CENTER.Y - 106
	ufo.speed = 2
	ufo.enterFrame = moveMyStuff
	ufo.startPosition = { x = ufo.x , y = ufo.y}
	parent:insert( ufo )

	--將音樂指定到第八頻道進行播放，並設定為不斷重複
	audio.play(  bgMusic , {channel = 8 , loops = -1 } )
	Runtime:addEventListener("enterFrame" , img_cloud1)
	Runtime:addEventListener("enterFrame" , img_cloud2)
	Runtime:addEventListener("enterFrame" , img_cloud3)
	Runtime:addEventListener("enterFrame" , img_road1)
	Runtime:addEventListener("enterFrame" , img_road2)
	Runtime:addEventListener("enterFrame" , img_building1)
	Runtime:addEventListener("enterFrame" , img_building2)

end

playButtonPressed = function ( event )
	if ("began" == event.phase) then
		audio.play( buttonPressedSound )
	elseif ("ended" == event.phase) then
		--轉移場景，開始玩
		composer.gotoScene( "gameplay" , changeSceneEffect )
	end
end

moreAppButtonPressed = function ( event )
	if ("began" == event.phase) then
		audio.play( buttonPressedSound )
	elseif ("ended" == event.phase) then
		--轉移場景，秀出更多App
	end
end

gameCenterButtonPressed = function ( event )
	if ("began" == event.phase) then
		audio.play( buttonPressedSound )
	elseif ("ended" == event.phase) then
		--轉移場景，秀出GameCenter成績
	end
end

ufoMove1 = function (  )
	local moveBack = function ( )
		ufo[2].isVisible = false
		stopMovingUfo = transition.to( ufo, { time = 1900 , y = ufo.startPosition.y - iPhone5AddOn , xScale = 1 , yScale = 1 , rotation = 0 , alpha = 1 , transition = easing.inQuad , onComplete = ufoMove1 } )
		
	end
	audio.play( goingUpLongSound )
	ufo[2].isVisible = true
	stopMovingUfo = transition.to( ufo, {time = 3000 , y = 180 - iPhone5AddOn , xScale = 0.8 , yScale = 0.8 , transition = easing.inQuad , onComplete = moveBack} )
end

ufoMove2 = function (  )
	local speed = 0
	if (ufo.x >= _SCREEN.CENTER.X) then
		speed = 4
	else 
		speed = 2
	end
	ufo.x = ufo.x - speed
	if ufo.x <= -ufo.width/2 then
		ufo.x = _SCREEN.WIDTH + ufo.width / 2
	end
end

moveMyStuff = function ( self , event )
	local speed = 0
	if (self.x >= _SCREEN.CENTER.X) then
		speed = self.speed * 4
	else 
		speed = self.speed
	end
	self.x = self.x - speed
	if (self.x <= -self.width/2) then
		self.x = display.contentWidth + self.width/2
	end
end

moveMyStuffNormal = function ( self , event )
	self.x = self.x - self.speed
	if (self.x <= -self.width/2) then
		self.x = display.contentWidth + self.width/2
	end
end
--=======================================================================================
--Composer
--=======================================================================================

--畫面沒到螢幕上時，先呼叫scene:create
--任務:負責UI畫面繪製
function scene:create(event)
	--把場景的view存在sceneGroup這個變數裡
	local sceneGroup = self.view
	initial(sceneGroup)
	--接下來把會出現在畫面的東西，加進sceneGroup裡面
end


--畫面到螢幕上時，呼叫scene:show
--任務:移除前一個場景，播放音效，開始計時，播放各種動畫
function  scene:show( event)
	local sceneGroup = self.view
	local phase = event.phase

	if( "will" == phase ) then
		--畫面即將要推上螢幕時要執行的程式碼寫在這邊
	elseif ( "did" == phase ) then
		--把畫面已經被推上螢幕後要執行的程式碼寫在這邊
		--可能是移除之前的場景，播放音效，開始計時，播放各種動畫
		ufoMove1()
		--Runtime:addEventListener( "enterFrame", ufoMove2 )

		--當加入的第二個參數為表格時，會自動尋找表格內與事件同名的屬性
		--Runtime:addEventListener("enterFrame" , ufo)
	end
end


--即將被移除，呼叫scene:hide
--任務:停止音樂，釋放音樂記憶體，停止移動的物體等
function scene:hide( event )
	
	local sceneGroup = self.view
	local phase = event.phase

	if ( "will" == phase ) then
		--畫面即將移開螢幕時，要執行的程式碼
		--這邊需要停止音樂，釋放音樂記憶體，有timer的計時器也可以在此停止

		transition.cancel( stopMovingUfo )
		Runtime:removeEventListener( "enterFrame", img_cloud1 )
		Runtime:removeEventListener( "enterFrame", img_cloud2 )
		Runtime:removeEventListener( "enterFrame", img_cloud3 )
		Runtime:removeEventListener( "enterFrame", img_building1 )
		Runtime:removeEventListener( "enterFrame", img_building2 )
		Runtime:removeEventListener( "enterFrame", img_road1 )
		Runtime:removeEventListener( "enterFrame", img_road2 )
		audio.stop( )
		audio.dispose( bgMusic )
		audio.dispose( buttonPressedSound )
		audio.dispose( goingUpLongSound )
		bgMusic = nil
		buttonPressedSound = nil
		goingUpLongSound = nil
	elseif ( "did" == phase ) then
		--畫面已經移開螢幕時，要執行的程式碼
	end
end

--下一個場景畫面推上螢幕後
--任務:摧毀場景
function scene:destory( event )
	if ("will" == event.phase) then
		--這邊寫下畫面要被消滅前要執行的程式碼

	end
end

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destory", scene )

return scene