--檢測是否為iPhone5以上的裝置
local thisDeviceHeight = 960
if(system.getInfo("model") == "iPhone" or system.getInfo("model") == "iPod touch") then
	local isIphone5 = (display.pixelHeight > 960)
	if isIphone5 then
		thisDeviceHeight = 1136
	end
end

application =
{

	content =
	{
		width = 640,
		height = thisDeviceHeight, 
		scale = "letterBox",
		fps = 30,
		
		--[[
		imageSuffix =
		{
			    ["@2x"] = 2,
		},
		--]]
	},

	--[[
	-- Push notifications
	notification =
	{
		iphone =
		{
			types =
			{
				"badge", "sound", "alert", "newsstand"
			}
		}
	},
	--]]    
}
